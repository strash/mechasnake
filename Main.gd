extends Node2D


onready var FoodScene: PackedScene = load("res://scene/Food.tscn") as PackedScene
onready var Snake: Node2D = get_node("SnakeLayer/Snake") as Node2D
onready var camera: Camera2D = get_node("Camera2D") as Camera2D
onready var direction: Node2D = get_node("SnakeLayer/Direction") as Node2D
onready var arrow: Sprite = get_node("SnakeLayer/Direction/Sprite") as Sprite


var overlay_alpha: float = 0.0
var self_paused: bool = false
var death: bool = false
var sound: bool = true

var food_pos: Vector2


func _ready() -> void:
	randomize()
	spawn_food()
	var _on_Snake_set_score_data = Snake.connect("set_score_data", self, "_on_Snake_set_score_data")
	var _on_Snake_set_keycap_data = Snake.connect("set_keycap_data", self, "_on_Snake_set_keycap_data")
	var _on_Snake_show_death_screen = Snake.connect("show_death_screen", self, "_on_Snake_show_death_screen")
	var _on_Death_reload_game = $DeathLayer/Death.connect("reload_game", self, "_on_Death_reload_game")


func _process(_delta: float) -> void:
	# set camera position
	var head_pos: Vector2 = Snake.SNAKE.body[0].get_position()
	camera.position = head_pos
	camera.smoothing_speed = Snake.SNAKE.body[0].tier
	_set_direction(head_pos)

	if not death and not $DeathLayer/Death.is_shown:
		if InputEventKey and Input.is_action_just_pressed("ui_space"):
			if get_tree().paused:
				get_tree().paused = false
				self_paused = false
				$HUDLayer/HUD/SpaceToPause.text = "PRESS SPACE TO PAUSE"
				if sound:
					$AudioStreamPlayer.play()
			else:
				get_tree().paused = true
				self_paused = true
				$HUDLayer/HUD/SpaceToPause.text = "PRESS SPACE TO RESUME"
				$AudioStreamPlayer.stop()
		if self_paused and overlay_alpha < 1.0:
			overlay_alpha += 0.05
		elif not self_paused and overlay_alpha > 0.0:
			overlay_alpha -= 0.05
		$HUDLayer/HUD/ColorRect.modulate.a = overlay_alpha

	if InputEventKey and Input.is_action_just_pressed("ui_f"):
		if sound:
			$AudioStreamPlayer.stop()
			$HUDLayer/HUD/FToMute.text = "PRESS F TO UNMUTE"
		else:
			$AudioStreamPlayer.play()
			$HUDLayer/HUD/FToMute.text = "PRESS F TO MUTE"
		sound = not sound


func spawn_food() -> void:
	var offset = 30
	var food = FoodScene.instance()
	var grid_size = GlobalVars.GRID * GlobalVars.CELL

	var x = convert(rand_range(0, grid_size.x - GlobalVars.CELL.x), TYPE_INT)
	var y = convert(rand_range(0, grid_size.y - GlobalVars.CELL.y), TYPE_INT)
	food.position.x = x - x % convert(GlobalVars.CELL.x, TYPE_INT) + offset
	food.position.y = y - y % convert(GlobalVars.CELL.y, TYPE_INT)
	food.connect("was_eaten", self, "_on_Food_was_eaten")
	food.camera = camera
	$SnakeLayer.call_deferred("add_child", food)
	food_pos = food.position


func _set_direction(pos) -> void:
	var distance = food_pos - pos
	direction.position = pos
	direction.position.y += 16.0
	var angle = rad2deg(atan2(distance.y, distance.x))
	arrow.set_rotation_degrees(angle)


func _on_Food_was_eaten(pos: Vector2) -> void:
	$BgPlateLayer/Plate.set_wave(pos, GlobalVars.COLORS[GlobalVars.COLOR.GOLD])
	spawn_food()
	Snake.increase_tier()


func _on_Snake_set_score_data(data: int) -> void:
	$HUDLayer/HUD.set_score_text(data)


func _on_Snake_set_keycap_data(data: Array) -> void:
	$HUDLayer/HUD.set_keycap_text(data)


func _on_Snake_show_death_screen() -> void:
	death = true
	var score = $HUDLayer/HUD.score
	$DeathLayer/Death.show_view(score)


func _on_Death_reload_game() -> void:
	Snake.SNAKE.speed = 100
	Snake.SNAKE.direction = GlobalVars.BODY.RIGHT
	Snake.new_direction = GlobalVars.BODY.RIGHT
	Snake.SNAKE.position = Vector2(570, 324)
	for i in Snake.SNAKE.body:
		i.queue_free()
	Snake.SNAKE.body.clear()
	Snake._ready()
	Snake.tick = 0.0

	var hud = $HUDLayer/HUD
	hud.score = 0
	hud._ready()

	$DeathLayer/Death.hide_view()

	overlay_alpha = 0.0
	self_paused = false
	death = false
