extends Node2D


signal set_score_data
signal set_keycap_data
signal show_death_screen


const KeyCap: PackedScene = preload("res://scene/KeyCap.tscn")


onready var eat_audio: AudioStreamPlayer = get_node("EatAudio") as AudioStreamPlayer
onready var death_audio: AudioStreamPlayer = get_node("DeathAudio") as AudioStreamPlayer
onready var tween: Tween = get_node("Tween") as Tween


var SNAKE: Dictionary = {
	"body": [], # array of keycaps
	"speed": 100.0, # snake speed
	"speed_step": 150, # speed increment
	"direction": GlobalVars.BODY.RIGHT, # starting direction
	"position": Vector2(570, 324), # starting position
	"length": 5, # starting length
}

var tick: float = 0.0
var tick_max: float = 100.0
var new_direction: int = GlobalVars.BODY.RIGHT


# BUILTINS - - - - - - - - -


func _ready() -> void:
	# factory
	for i in SNAKE.length:
		var keycap: Area2D = KeyCap.instance() as Area2D
		keycap.set("KEY_TYPE", GlobalVars.KEY_TYPE.REGULAR)
		keycap.set("BODY_PART", i)
		keycap.set("tier", GlobalVars.START_TIER)
		keycap.position.x = SNAKE.position.x - GlobalVars.CELL.x * i
		keycap.position.y = SNAKE.position.y
		self.add_child(keycap)
		SNAKE.body.append(keycap)
	emit_signal("set_keycap_data", _get_score_data())


func _process(delta: float) -> void:
	tick += delta * SNAKE.speed as float
	if tick >= tick_max:
		# check collisions
		if collision_wall(new_direction) or collision_self():
			death()
		else:
			# set speed
			SNAKE.speed = SNAKE.speed_step * SNAKE.body[0].tier
			# set position
			for i in range(SNAKE.length - 1, 0, -1): # iterating from tail to head
				set_snake_position(SNAKE.body[i], SNAKE.body[i].position, SNAKE.body[i - 1].position, tick_max / SNAKE.speed)
			# set direction
			set_snake_direction(new_direction, tick_max / SNAKE.speed)
		tick = 0
	# set z-index
	for i in get_tree().get_nodes_in_group("zindex_group"):
		i.set_entity_z_index()


func _input(event: InputEvent) -> void:
	if event and InputEventKey:
		if Input.is_action_pressed("ui_right"):
			new_direction = GlobalVars.BODY.RIGHT
			_animate_keypress()
		if Input.is_action_pressed("ui_left"):
			new_direction = GlobalVars.BODY.LEFT
			_animate_keypress()
		if Input.is_action_pressed("ui_up"):
			new_direction = GlobalVars.BODY.UP
			_animate_keypress()
		if Input.is_action_pressed("ui_down"):
			new_direction = GlobalVars.BODY.DOWN
			_animate_keypress()


# METHODS - - - - - - - - -


# snake head position and direction
func set_snake_direction(direction: int, duration: float) -> void:
	var head: Area2D = SNAKE.body[0]
	var new_pos: Vector2 = head.position
	if SNAKE.direction == GlobalVars.BODY.RIGHT:
		if direction == GlobalVars.BODY.LEFT or direction == GlobalVars.BODY.UP:
			new_pos.y = new_pos.y - GlobalVars.CELL.y
			SNAKE.direction = GlobalVars.BODY.UP
		elif direction == GlobalVars.BODY.DOWN:
			new_pos.y = new_pos.y + GlobalVars.CELL.y
			SNAKE.direction = direction
		else:
			new_pos.x = new_pos.x + GlobalVars.CELL.x
	elif SNAKE.direction == GlobalVars.BODY.LEFT:
		if direction == GlobalVars.BODY.RIGHT or direction == GlobalVars.BODY.UP:
			new_pos.y = new_pos.y - GlobalVars.CELL.y
			SNAKE.direction = GlobalVars.BODY.UP
		elif direction == GlobalVars.BODY.DOWN:
			new_pos.y = new_pos.y + GlobalVars.CELL.y
			SNAKE.direction = direction
		else:
			new_pos.x = new_pos.x - GlobalVars.CELL.x
	elif SNAKE.direction == GlobalVars.BODY.UP:
		if direction == GlobalVars.BODY.RIGHT or direction == GlobalVars.BODY.DOWN:
			new_pos.x = new_pos.x + GlobalVars.CELL.x
			SNAKE.direction = GlobalVars.BODY.RIGHT
		elif direction == GlobalVars.BODY.LEFT:
			new_pos.x = new_pos.x - GlobalVars.CELL.x
			SNAKE.direction = direction
		else:
			new_pos.y = new_pos.y - GlobalVars.CELL.y
	elif SNAKE.direction == GlobalVars.BODY.DOWN:
		if direction == GlobalVars.BODY.LEFT or direction == GlobalVars.BODY.UP:
			new_pos.x = new_pos.x - GlobalVars.CELL.x
			SNAKE.direction = GlobalVars.BODY.LEFT
		elif direction == GlobalVars.BODY.RIGHT:
			new_pos.x = new_pos.x + GlobalVars.CELL.x
			SNAKE.direction = direction
		else:
			new_pos.y = new_pos.y + GlobalVars.CELL.y
	set_snake_position(head, head.position, new_pos, duration)


# snake body part position
# assignment of a position from the previous body part
func set_snake_position(key: Area2D, from: Vector2, to: Vector2, duration: float) -> void:
	var key_tween: Tween = key.get_node("Tween")
	var _t: int = key_tween.interpolate_property(key, "position", from, to, duration)
	var _start: bool = key_tween.start()


func increase_tier() -> void:
	var total_tier: float = 0.0
	for i in SNAKE.body:
		if i.KEY_TYPE == GlobalVars.KEY_TYPE.REGULAR:
			if i.BODY_PART == new_direction:
				i.eat_food(GlobalVars.COLOR.GOLD)
				emit_signal("set_score_data", i.tier)
			if i.BODY_PART != GlobalVars.BODY.HEAD:
				total_tier += i.tier
	# encreasing head tier
	var head_tier: int = floor(total_tier / 4.0) as int
	SNAKE.body[0].increase_keycap_tier(head_tier)
	emit_signal("set_keycap_data", _get_score_data())
	eat_audio.play()


# collision with walls
func collision_wall(direction: int) -> bool:
	var head_pos: Vector2 = SNAKE.body[0].position - GlobalVars.CELL / 2
	head_pos.y += 16
	var x: float = GlobalVars.GRID.x * GlobalVars.CELL.x
	var y: float = GlobalVars.GRID.y * GlobalVars.CELL.y
	var head_x: float = head_pos.x + GlobalVars.CELL.x
	var head_y: float = head_pos.y + GlobalVars.CELL.y
	return (head_pos.x <= 0.1 and direction == GlobalVars.BODY.LEFT) or (head_pos.y <= 0.1 and direction == GlobalVars.BODY.UP) or (head_x >= x and direction == GlobalVars.BODY.RIGHT) or (head_y >= y and direction == GlobalVars.BODY.DOWN)


# self collition
func collision_self() -> bool:
	var head_pos: Vector2 = SNAKE.body[0].position
	for i in range(1, SNAKE.length):
		if head_pos.x == SNAKE.body[i].position.x and head_pos.y == SNAKE.body[i].position.y:
			return true
	return false


# game over
func death() -> void:
	var _t: int
	SNAKE.speed = 0
	for i in SNAKE.body:
		_t = tween.interpolate_property(i, "modulate:a", 1, 0.6, 0.6, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		i.get_node("AnimationPlayer").stop()
	var _start = tween.start()
	emit_signal("show_death_screen")
	death_audio.play()


func _animate_keypress() -> void:
	for i in SNAKE.body:
		if i.has_method("animate_keypress") and i.BODY_PART == new_direction:
			i.animate_keypress()


func _get_score_data() -> Array:
	var score_data: Array = []
	for i in SNAKE.length:
		score_data.append({
			"from": SNAKE.body[i].food_count,
			"to": SNAKE.body[i].tier,
			"tier": GlobalVars.TIER_MAP[SNAKE.body[i].tier]
		})
	return score_data


# SIGNALS - - - - - - - - -
