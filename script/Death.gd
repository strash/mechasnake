extends ColorRect


signal reload_game


var score:= 0
var is_shown = false

onready var anim_player:AnimationPlayer = get_node("AnimationPlayer")
onready var tween:Tween = $Tween
onready var score_label:Label = $Center/Score


func _ready() -> void:
	anim_player.stop()


func _process(_delta: float) -> void:
	score_label.text = "SCORE: " + str(round(score))


func _input(event: InputEvent) -> void:
	if event and InputEventKey and Input.is_action_just_pressed("ui_space") and is_shown:
		emit_signal("reload_game")


func show_view(new_score: int) -> void:
	is_shown = true
	anim_player.play("space_idle")
	var _t = tween.interpolate_property(self, "modulate:a", 0, 1, 0.8)
	if not tween.is_active():
		var _start = tween.start()
	yield(get_tree().create_timer(0.8), "timeout")
	_set_score(new_score)


func hide_view() -> void:
	score = 0
	anim_player.stop()
	var _t = tween.interpolate_property(self, "modulate:a", 1, 0, 0.3)
	if not tween.is_active():
		var _start = tween.start()
	yield(get_tree().create_timer(0.3), "timeout")
	is_shown = false


func _set_score(new_score: int) -> void:
	var _t = tween.interpolate_property(self, "score", score, new_score, 2)
	if not tween.is_active():
		var _start = tween.start()
