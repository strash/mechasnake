extends Area2D


var KEY_TYPE: int # type of the key
var BODY_PART: int # the part of the snake body
var tier: int # level
var food_count: int = 0 # counter of all eaten food by the key


func _ready() -> void:
	($AnimationPlayer as AnimationPlayer).playback_speed = self.tier
	set_entity_z_index()
	set_key_color(self.tier)
	# set textures by tier
	match BODY_PART:
		GlobalVars.BODY.HEAD:
			($AnimationPlayer as AnimationPlayer).play("head_idle") # animate head
			($KeyCap/DecalTop as Sprite).texture = load("res://assets/image/keycap/decal_top/head.png") as Texture
			($KeyCap/DecalBottom as Sprite).texture = load("res://assets/image/keycap/decal_bottom/head.png") as Texture
			var collision: CollisionShape2D = CollisionShape2D.new()
			var collision_shape: RectangleShape2D = RectangleShape2D.new()
			collision_shape.extents = Vector2(30.0, 20.0)
			collision.shape = collision_shape
			collision.position.y = 16.0
			self.add_child(collision)
		GlobalVars.BODY.UP:
			($KeyCap/DecalTop as Sprite).texture = load("res://assets/image/keycap/decal_top/up.png") as Texture
			($KeyCap/DecalBottom as Sprite).texture = load("res://assets/image/keycap/decal_bottom/arrow_up.png") as Texture
		GlobalVars.BODY.DOWN:
			($KeyCap/DecalTop as Sprite).texture = load("res://assets/image/keycap/decal_top/down.png") as Texture
			($KeyCap/DecalBottom as Sprite).texture = load("res://assets/image/keycap/decal_bottom/arrow_down.png") as Texture
		GlobalVars.BODY.LEFT:
			($KeyCap/DecalTop as Sprite).texture = load("res://assets/image/keycap/decal_top/left.png") as Texture
			($KeyCap/DecalBottom as Sprite).texture = load("res://assets/image/keycap/decal_bottom/arrow_left.png") as Texture
		GlobalVars.BODY.RIGHT:
			($KeyCap/DecalTop as Sprite).texture = load("res://assets/image/keycap/decal_top/right.png") as Texture
			($KeyCap/DecalBottom as Sprite).texture = load("res://assets/image/keycap/decal_bottom/arrow_right.png") as Texture


func set_entity_z_index() -> void:
	self.z_index = self.position.y as int


func animate_keypress() -> void:
	($AnimationPlayer as AnimationPlayer).play("key_press")


func _set_decal_top(head_color: int, other_color: int) -> void:
	var color: Color = GlobalVars.COLORS[head_color] if BODY_PART == GlobalVars.BODY.HEAD else GlobalVars.COLORS[other_color]
	color.a = 0.8
	var d: Sprite = $KeyCap/DecalTop as Sprite
	var _t: int = ($Tween as Tween).interpolate_property(d, "modulate", d.modulate, color, 0.2)


func _set_decal_bottom(other_color: int) -> void:
	var color: Color = GlobalVars.COLORS[other_color]
	color.a = 0.8
	var d: Sprite = $KeyCap/DecalBottom as Sprite
	var _t: int = ($Tween as Tween).interpolate_property(d, "modulate", d.modulate, color, 0.2)


func _set_shadow(color: Color) -> void:
	var s: Sprite = $Shadow as Sprite
	var _t: int = ($Tween as Tween).interpolate_property(s, "modulate", s.modulate, color, 0.2)


func set_key_color(color_index: int) -> void:
	var _t: int
	_t = ($Tween as Tween).interpolate_property($KeyCap.material, "shader_param/color", $KeyCap.material.get_shader_param("color"), GlobalVars.COLORS[color_index], 0.2)
	var shadow_a: int = round(70.0 * 255.0 / 100.0) as int
	match color_index:
		GlobalVars.COLOR.LIGHT:
			_set_decal_top(GlobalVars.COLOR.DARK, GlobalVars.COLOR.DARK)
			_set_decal_bottom(GlobalVars.COLOR.DARK)
			_set_shadow(Color8(28, 28, 32, shadow_a))
		GlobalVars.COLOR.GREY:
			_set_decal_top(GlobalVars.COLOR.DARKGREY, GlobalVars.COLOR.GREY)
			_set_decal_bottom(GlobalVars.COLOR.DARK)
			_set_shadow(Color8(28, 28, 32, shadow_a))
		GlobalVars.COLOR.DARKGREY:
			_set_decal_top(GlobalVars.COLOR.DARK, GlobalVars.COLOR.DARKGREY)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(28, 28, 32, shadow_a))
		GlobalVars.COLOR.DARK:
			_set_decal_top(GlobalVars.COLOR.GREY, GlobalVars.COLOR.DARK)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(28, 28, 32, shadow_a))
		GlobalVars.COLOR.PINK:
			_set_decal_top(GlobalVars.COLOR.WHITE, GlobalVars.COLOR.PINK)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(124, 5, 25, shadow_a))
		GlobalVars.COLOR.BLUE:
			_set_decal_top(GlobalVars.COLOR.WHITE, GlobalVars.COLOR.BLUE)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(6, 20, 160, shadow_a))
		GlobalVars.COLOR.GREEN:
			_set_decal_top(GlobalVars.COLOR.WHITE, GlobalVars.COLOR.GREEN)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(2, 39, 8, shadow_a))
		GlobalVars.COLOR.ORANGE:
			_set_decal_top(GlobalVars.COLOR.WHITE, GlobalVars.COLOR.ORANGE)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(186, 12, 1, shadow_a))
		GlobalVars.COLOR.GOLD:
			_set_decal_top(GlobalVars.COLOR.WHITE, GlobalVars.COLOR.GOLD)
			_set_decal_bottom(GlobalVars.COLOR.WHITE)
			_set_shadow(Color8(181, 83, 0, shadow_a))
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func eat_food(max_tier: int) -> void:
	self.food_count += 1
	if self.food_count >= self.tier and self.tier < max_tier:
		self.food_count = 0
		self.tier += 1
		increase_keycap_tier(self.tier)


func increase_keycap_tier(next_tier: int) -> void:
	set_key_color(next_tier)
	if self.BODY_PART == GlobalVars.BODY.HEAD:
		self.tier = next_tier
		($AnimationPlayer as AnimationPlayer).playback_speed = next_tier
	else:
		($AnimationPlayer as AnimationPlayer).playback_speed = next_tier as float / 2
