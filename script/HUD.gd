extends Control


onready var SCORE: Label = get_node("Score")
onready var tween: Tween = get_node("Tween")
onready var BODY: Array = [
	{
		"Score": get_node("VBoxContainer/Head/V/Score") as Label,
		"Tier": get_node("VBoxContainer/Head/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/W/V/Score") as Label,
		"Tier": get_node("VBoxContainer/W/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/S/V/Score") as Label,
		"Tier": get_node("VBoxContainer/S/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/A/V/Score") as Label,
		"Tier": get_node("VBoxContainer/A/V/Tier") as Label,
	},
	{
		"Score": get_node("VBoxContainer/D/V/Score") as Label,
		"Tier": get_node("VBoxContainer/D/V/Tier") as Label,
	},
]

var score:= 0


func _ready() -> void:
	var j = 0
	for i in BODY:
		if j == 0:
			i.Score.text = "1 tier"
		else:
			i.Score.text = "0/1"
		i.Tier.text = "LIGHT"
		i.Tier.set("custom_colors/font_color", GlobalVars.COLORS[GlobalVars.COLOR.LIGHT])
		j += 1
	SCORE.text = str(score)


func _process(_delta: float) -> void:
	SCORE.text = str(round(score))


func set_keycap_text(data: Array) -> void:
	var j = 0
	for i in BODY:
		if j == 0:
			i.Score.text = "%s tier" % str(data[j].to)
		else:
			if data[j].to == GlobalVars.COLOR.GOLD:
				i.Score.text = "max"
			else:
				i.Score.text = "%s\/%s" % [str(data[j].from), str(data[j].to)]
		i.Tier.text = str(data[j].tier)
		i.Tier.set("custom_colors/font_color", GlobalVars.COLORS[data[j].to])
		j += 1


func set_score_text(increase: int) -> void:
	var _t = tween.interpolate_property(self, "score", score, score + increase, 0.3)
	if not tween.is_active():
		var _start = tween.start()
