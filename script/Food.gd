extends Node2D


class_name Food


onready var particle: CPUParticles2D = get_node("CPUParticles2D")
onready var anim_player: AnimationPlayer = get_node("AnimationPlayer")
onready var tween: Tween = get_node("Tween")


signal was_eaten


var camera: Camera2D


func _ready() -> void:
	set_entity_z_index()
	anim_player.play("sphere_show")
	yield(get_tree().create_timer(0.2), "timeout")
	anim_player.play("sphere_idle")
	particle.emitting = true


func set_entity_z_index() -> void:
	self.z_index = convert(self.position.y, TYPE_INT)


func _on_Food_area_entered(_area) -> void:
	emit_signal("was_eaten", position)
	$CollisionShape2D.set_deferred("disabled", true)
	$CollisionShape2D.queue_free()
	anim_player.play("sphere_eaten")
	var score_label_pos = Vector2(0.0, 0.0) - (self.position - (camera.get_camera_screen_center() - Vector2(640.0 - 20.0, 400.0 - 20.0)))
	var _t1 = tween.interpolate_property(particle, "position", particle.position, score_label_pos, 1.5, tween.TRANS_CUBIC, tween.EASE_OUT)
	var _t2 = tween.interpolate_property(particle, "modulate:a", 1.0, 0.0, 1.0)
	var _start = tween.start()
	yield(get_tree().create_timer(1.5), "timeout")
	queue_free()
