extends Node


enum KEY_TYPE { REGULAR, MODIFIER } # key type
enum BODY { HEAD, UP, DOWN, LEFT, RIGHT } # body part/direction constants
enum COLOR { WHITE, LIGHT, GREY, DARKGREY, DARK, PINK, BLUE, GREEN, ORANGE, GOLD } # colors

const COLORS: Dictionary = {
	0: Color8(255, 255, 255), # WHITE
	1: Color8(211, 218, 224), # LIGHT
	2: Color8(173, 175, 184), # GREY
	3: Color8(116, 119, 128), # DARKGREY
	4: Color8(47, 48, 54), # DARK
	5: Color8(178, 29, 74), # PINK
	6: Color8(41, 73, 202), # BLUE
	7: Color8(13, 92, 33), # GREEN
	8: Color8(206, 55, 24), # ORANGE
	9: Color8(221, 144, 1), # GOLD
}

const TIER_MAP: Dictionary = { # map of tier names for hud
	1: "LIGHT",
	2: "GREY",
	3: "DARK GREY",
	4: "DARK",
	5: "PINK",
	6: "BLUE",
	7: "GREEN",
	8: "ORANGE",
	9: "GOLD",
}
const START_TIER: int = 1

const GRID: Vector2 = Vector2(26, 26) # grid
const CELL: Vector2 = Vector2(60, 40) # grid cell size
