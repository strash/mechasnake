extends Node2D


var radius: float = 0.0
var wave_active: bool = false
var tick: float = 0.05


# BUILTINS - - - - - - - - -


func _process(_delta: float) -> void:
	if wave_active:
		$Base.material.set_shader_param("radius", radius)
		radius += tick
		if radius >= 1.0:
			$Base.material.set_shader_param("active", false)
			wave_active = false


# METHODS - - - - - - - - -


func set_wave(origin: Vector2, color: Color) -> void:
	radius = 0.0
	wave_active = true
	$Base.material.set_shader_param("origin", origin / (GlobalVars.GRID * GlobalVars.CELL))
	$Base.material.set_shader_param("wave_color", color)
	$Base.material.set_shader_param("active", true)


# SIGNALS - - - - - - - - -
