shader_type canvas_item;

uniform vec4 color : hint_color;
uniform sampler2D light_map;


float b_soft_light(float base, float blend) {
	return (1.0 - 2.0 * blend) * (base * base) + 2.0 * blend * base;
}

float b_overlay(float base, float blend) {
	if (base < 0.5) {
		return 2.0 * base * blend;
	} else {
		return 1.0 - 2.0 * (1.0 - base) * (1.0 - blend);
	}
}

vec3 blendOverlay(vec3 base, vec3 blend) {
	return vec3(b_overlay(base.r, blend.r), b_overlay(base.g, blend.g), b_overlay(base.b, blend.b));
}

void fragment() {
	vec4 normal_color = texture(light_map, UV);
	vec3 blended_color = blendOverlay(color.rgb, normal_color.rgb);
	COLOR = vec4(blended_color, texture(TEXTURE, UV).a);
}