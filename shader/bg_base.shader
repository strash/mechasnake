shader_type canvas_item;
render_mode unshaded;

uniform vec2 origin = vec2(0.5); // wave origin
uniform vec4 wave_color: hint_color = vec4(1.0); // wave color
uniform float radius = 0.0;
uniform bool active = false; // state

const vec4 BG_COLOR = vec4(0.0, 0.0, 0.0, 1.0);

float circle_shape(vec2 position, float r, float blur) {
	return smoothstep(r - blur, r, length(position - origin));
}

void fragment() {
	if (active) {
		float circle_b = circle_shape(UV, radius, radius); // wave color
		float circle_t = circle_shape(UV, radius - 0.06, radius); // background
		COLOR = mix(BG_COLOR, vec4(wave_color.rgb, 1.0 - radius), circle_t - circle_b);
	} else {
		COLOR = BG_COLOR;
	}
}