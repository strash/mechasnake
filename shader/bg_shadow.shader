shader_type canvas_item;

const vec4 VIGNETTE = vec4(0.0, 0.0, 0.0, 0.2);
const vec4 TINT = vec4(15.0 / 255.0, 61.0 / 255.0, 125.0 / 255.0, 0.1);

float circle_shape(vec2 position, float r, float blur) {
	return smoothstep(r - blur, r, length(position - vec2(0.5)));
}

void fragment() {
	vec4 color = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0);
	color = mix(color, VIGNETTE, circle_shape(UV, 1.0, 0.8));
	color = mix(color, TINT, 0.1);
	COLOR = color;
}