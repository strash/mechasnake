shader_type canvas_item;

const vec2 GRID = vec2(26.0); // grid

void fragment() {
	COLOR = texture(TEXTURE, UV * GRID);
}